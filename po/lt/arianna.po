# Lithuanian translations for arianna package.
# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the arianna package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: arianna\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-04 00:44+0000\n"
"PO-Revision-Date: 2023-02-20 00:54+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: content/ui/EpubViewerPage.qml:120 content/ui/main.qml:160
#, kde-format
msgid "No search results"
msgstr ""

#: content/ui/EpubViewerPage.qml:128 content/ui/EpubViewerPage.qml:238
#: content/ui/EpubViewerPage.qml:242
#, kde-format
msgid "Loading"
msgstr ""

#: content/ui/EpubViewerPage.qml:145
#, kde-format
msgid "No book selected"
msgstr ""

#: content/ui/EpubViewerPage.qml:148
#, kde-format
msgid "Open file"
msgstr ""

#: content/ui/EpubViewerPage.qml:169 content/ui/main.qml:332
#, kde-format
msgid "Please choose a file"
msgstr ""

#: content/ui/EpubViewerPage.qml:170 content/ui/main.qml:333
#, kde-format
msgctxt "Name filter for EPUB files"
msgid "eBook files (*.epub *.cb* *.fb2 *.fb2zip)"
msgstr ""

#: content/ui/EpubViewerPage.qml:206
#, kde-format
msgid "Copy"
msgstr ""

#: content/ui/EpubViewerPage.qml:212
#, kde-format
msgid "Find"
msgstr ""

#: content/ui/EpubViewerPage.qml:223
#, kde-format
msgctxt "Book reading progress"
msgid "%1%"
msgstr ""

#: content/ui/EpubViewerPage.qml:237
#, kde-format
msgid "Time left in chapter:"
msgstr ""

#: content/ui/EpubViewerPage.qml:241
#, kde-format
msgid "Time left in book:"
msgstr ""

#: content/ui/EpubViewerPage.qml:252
#, kde-format
msgid "Previous Page"
msgstr ""

#: content/ui/EpubViewerPage.qml:268
#, kde-format
msgid "Next Page"
msgstr ""

#: content/ui/GridBrowserDelegate.qml:193
#, kde-format
msgctxt "should be keep short, inside a label. Will be in uppercase"
msgid "New"
msgstr ""

#: content/ui/LibraryPage.qml:18
#, kde-format
msgid "Library"
msgstr ""

#: content/ui/LibraryPage.qml:87
#, kde-format
msgctxt "@info placeholder"
msgid "Add some books"
msgstr ""

#: content/ui/main.qml:17 content/ui/main.qml:288 main.cpp:73
#, kde-format
msgid "Arianna"
msgstr ""

#: content/ui/main.qml:200
#, kde-format
msgctxt "Switch to the listing page showing the most recently read books"
msgid "Home"
msgstr ""

#: content/ui/main.qml:204
#, kde-format
msgid "Home"
msgstr ""

#: content/ui/main.qml:207
#, kde-format
msgctxt "Switch to the listing page showing the most recently discovered books"
msgid "Recently Added Books"
msgstr ""

#: content/ui/main.qml:213
#, kde-format
msgctxt ""
"Open a book from somewhere on disk (uses the open dialog, or a drilldown on "
"touch devices)"
msgid "Open Other..."
msgstr ""

#: content/ui/main.qml:220
#, kde-format
msgctxt ""
"Heading for switching to listing page showing items grouped by some "
"properties"
msgid "Group By"
msgstr ""

#: content/ui/main.qml:223
#, kde-format
msgctxt "Switch to the listing page showing items grouped by author"
msgid "Author"
msgstr ""

#: content/ui/main.qml:229
#, kde-format
msgctxt "Switch to the listing page showing items grouped by series"
msgid "Series"
msgstr ""

#: content/ui/main.qml:231
#, kde-format
msgctxt "Title of the page with books grouped by what series they are in"
msgid "Group by Series"
msgstr ""

#: content/ui/main.qml:235
#, kde-format
msgctxt "Switch to the listing page showing items grouped by publisher"
msgid "Publisher"
msgstr ""

#: content/ui/main.qml:241
#, kde-format
msgctxt "Switch to the listing page showing items grouped by genres"
msgid "Keywords"
msgstr ""

#: content/ui/main.qml:243
#, kde-format
msgctxt "Title of the page with books grouped by genres"
msgid "Group by Genres"
msgstr ""

#: content/ui/main.qml:254
#, kde-format
msgctxt "Open the settings page"
msgid "Settings"
msgstr ""

#: content/ui/main.qml:311
#, kde-format
msgctxt "@action:button"
msgid "Add Book…"
msgstr ""

#: main.cpp:75
#, kde-format
msgid "EPub Reader"
msgstr ""

#: main.cpp:77
#, kde-format
msgid "2022 Niccolò Venerandi <niccolo@venerandi.com>"
msgstr ""

#: main.cpp:78
#, kde-format
msgid "Niccolò Venerandi"
msgstr ""

#: main.cpp:78
#, kde-format
msgid "Maintainer"
msgstr ""

#: main.cpp:79
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: main.cpp:79
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: main.cpp:92
#, kde-format
msgid "Epub reader"
msgstr ""

#: main.cpp:93
#, kde-format
msgid "Epub file to open"
msgstr ""
